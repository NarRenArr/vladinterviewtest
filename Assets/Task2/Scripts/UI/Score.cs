using System;
using TMPro;
using UnityEngine;

public class Score : Singleton<Score>
{
    public event Action<int> OnScoreChange;

    [SerializeField] private TextMeshProUGUI _text;

    private int _accumulatedScore;

    public void AddScore(int score)
    {
        _accumulatedScore += score;
        _text.text = "Score: " + _accumulatedScore;
    }
}
