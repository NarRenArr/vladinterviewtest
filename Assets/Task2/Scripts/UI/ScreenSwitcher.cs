using UnityEngine;

public class ScreenSwitcher : Singleton<ScreenSwitcher>
{
    [SerializeField] private GameObject _game;
    [SerializeField] private GameObject _pause;
    [SerializeField] private GameObject _start;
    [SerializeField] private GameObject _end;

    public void ShowGame() => ShowScreen(_game);
    public void ShowStart() => ShowScreen(_start);
    public void ShowEnd() => ShowScreen(_end);

    public void TryShowPauseScreen(bool isPaused)
    {
        SwitchOffAllScreens();
        _game.SetActive(!isPaused);
        _pause.SetActive(isPaused);
    }

    private void ShowScreen(GameObject screen)
    {
        SwitchOffAllScreens();
        screen.SetActive(true);
    }

    private void SwitchOffAllScreens()
    {
        _game.SetActive(false);
        _pause.SetActive(false);
        _start.SetActive(false);
        _end.SetActive(false);
    }

    private void OnEnable()
    {
        Player.Instance.OnDeath += ShowEnd;
        Pause.Instance.OnPause += TryShowPauseScreen;
        GameStarter.Instance.OnSceneStart += ShowStart;
        GameStarter.Instance.OnGameStart += ShowGame;
    }

    private void OnDisable()
    {
        Player.Instance.OnDeath -= ShowEnd;
        Pause.Instance.OnPause -= TryShowPauseScreen;
        GameStarter.Instance.OnSceneStart -= ShowStart;
        GameStarter.Instance.OnGameStart -= ShowGame;
    }
}
