using System;
using UnityEngine;

public class Pause : Singleton<Pause>
{
    public event Action<bool> OnPause;

    public void PauseHendler(bool isPaused)
    {
        if (isPaused)
            Time.timeScale = 0;
        else
            Time.timeScale = 1;

        OnPause(isPaused);
    }
}
