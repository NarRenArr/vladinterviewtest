using System;
using UnityEngine;

public class GravityInverter : Singleton<GravityInverter>
{
    public event Action OnGravityInvertion;

    [SerializeField] private KeyCode _gravityInvertionkey;

    private void Update()
    {
        if (Input.GetKeyDown(_gravityInvertionkey) && !Pointer.IsPointerOverUIElement())
            OnGravityInvertion();
    }
}
