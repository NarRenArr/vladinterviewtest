using UnityEngine;

public class Gravity : MonoBehaviour
{
    [SerializeField] private float _yForce;

    private Rigidbody _rb;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        ApplyGravity();
    }

    // � ����� ��� Time.deltaTime. FixedUpdate ��������� �� ����������, ������� � �� ��������� Time.deltaTime.
    private void ApplyGravity() => _rb.AddForce(Vector3.up * _yForce);

    private void InvertGravity() => _yForce *= -1;

    private void OnEnable()
    {
        GravityInverter.Instance.OnGravityInvertion += InvertGravity;
    }

    private void OnDisable()
    {
        GravityInverter.Instance.OnGravityInvertion -= InvertGravity;
    }
}
