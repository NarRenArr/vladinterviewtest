using System;
using UnityEngine;

public class Player : Singleton<Player>
{
    public event Action OnDeath;

    public void Die()
    {
        Time.timeScale = 0;
        OnDeath();
    }
}
