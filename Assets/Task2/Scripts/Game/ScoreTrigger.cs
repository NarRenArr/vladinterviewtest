using UnityEngine;

public class ScoreTrigger : MonoBehaviour
{
    [SerializeField] private int _score;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Player>())
            Score.Instance.AddScore(_score);    
    }
}
