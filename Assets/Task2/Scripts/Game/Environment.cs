using UnityEngine;

public class Environment : MonoBehaviour
{
    [SerializeField] private float _speed;

    void Update()
    {
        transform.position += Vector3.right * _speed * Time.deltaTime;
    }
}
