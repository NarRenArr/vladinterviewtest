using System;
using UnityEngine;

public class GameStarter : Singleton<GameStarter>
{
    public event Action OnSceneStart;
    public event Action OnGameStart;

    private void Start()
    {
        Time.timeScale = 0;
        OnSceneStart();
    }

    public void StartGame()
    {
        Time.timeScale = 1;
        OnGameStart();
    }
}
