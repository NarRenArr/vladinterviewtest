using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Pointer
{
    /// <summary>
    /// Returns true, if the cursor is over any UI element.
    /// </summary>
    /// <returns></returns>
    public static bool IsPointerOverUIElement()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }
}
