using UnityEngine;

namespace Task1 
{
    public class ListTester : MonoBehaviour
    {
        void Start()
        {
            List<int> list = new List<int>();

            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.ShowAll();
            list.ShowLenght();

            list.Add(5);
            list.Add(6);
            list.Add(7);
            list.Add(8);
            list.ShowAll();
            list.ShowLenght();

            list.RemoveFirst();
            list.RemoveFirst();
            list.RemoveFirst();
            list.RemoveFirst();
            list.RemoveFirst();
            list.RemoveFirst();
            list.RemoveFirst();
            list.RemoveFirst();
            list.RemoveFirst();
            list.Add(9);
            list.Add(10);
            list.Add(11);
            list.Add(12);
            list.ShowAll();
            list.ShowLenght();
        }
    }
}
