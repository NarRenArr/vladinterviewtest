using UnityEngine;

namespace Task1
{
    public class List<T>
    {
        private Item<T> _first;
        private Item<T> _last;

        public void Add(T value)
        {
            Item<T> newItem = new Item<T>();
            newItem.value = value;

            if (_first == null)
                _first = newItem;
            else
            {
                _last.nextItem = newItem;
                newItem.previousItem = _last;
            }

            _last = newItem;
        }

        public void RemoveFirst()
        {
            if (_first == null)
                Debug.LogWarning("You are trying to remove an item from an empty List.");
            else if (_first.nextItem == null)
                _first = null;
            else
            {
                _first = _first.nextItem;
                _first.previousItem = null;
            }
        }

        public void ShowAll()
        {
            Item<T> currentItem = _first;

            while (currentItem != null)
            {
                Debug.Log(currentItem.value);

                if (currentItem.nextItem == null)
                    break;
                else
                    currentItem = currentItem.nextItem;
            }
        }

        public void ShowLenght()
        {
            int length = 0;
            Item<T> currentItem = _first;

            while (currentItem != null)
            {
                length++;

                if (currentItem.nextItem == null)
                    break;
                else
                    currentItem = currentItem.nextItem;
            }

            Debug.Log("Length = " + length);
        }

        private class Item<T>
        {
            public T value;
            public Item<T> previousItem;
            public Item<T> nextItem;
        }
    }
}
